/*
 *  Copyright (C) 2017 Michael Bohlender, <michael.bohlender@kdemail.net>
 *  Copyright (C) 2017 Christian Mollekopf, <mollekopf@kolabsys.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.0 as Dialogs
import QtQuick.Controls 2

import org.kde.kirigami 2.2 as Kirigami
import org.kube.framework 1.0 as Kube

FocusScope {
    id: root

    property var pageRow
    property alias initialText: textEditor.initialText
    property var controller

    signal done()
    signal focusChange()

    Rectangle {
        anchors.fill: parent
        color: Kube.Colors.paperWhite

        ColumnLayout {
            anchors {
                top: parent.top
                bottom: parent.bottom
                margins: Kube.Units.largeSpacing
                horizontalCenter: parent.horizontalCenter
            }
            property var preferredWidth: Kube.Units.gridUnit * 24
            property var minimumWidth: root.width - (Kube.Units.largeSpacing + Kube.Units.gridUnit * 2) * 2
            width: Math.min(minimumWidth, preferredWidth)

            spacing: Kube.Units.smallSpacing

            Flow {
                id: attachments

                Layout.fillWidth: true
                layoutDirection: Qt.RightToLeft
                spacing: Kube.Units.smallSpacing
                clip: true

                Repeater {
                    model: controller.attachments.model
                    delegate: Kube.AttachmentDelegate {
                        name: model.filename ? model.filename : ""
                        icon: model.iconname ? model.iconname : ""
                        clip: true
                        actionIcon: Kube.Icons.remove
                        onExecute: controller.attachments.remove(model.id)
                    }
                }
            }

            RowLayout {

                spacing: Kube.Units.largeSpacing

                Row {
                    spacing: 1

                    Kube.IconButton {
                        iconName: Kube.Icons.bold
                        checkable: true
                        checked: textEditor.bold
                        onClicked: textEditor.bold = !textEditor.bold
                        focusPolicy: Qt.TabFocus
                        focus: false
                        opacity: activeFocus || hovered ? 1 : 0.6
                    }
                    Kube.IconButton {
                        iconName: Kube.Icons.italic
                        checkable: true
                        checked: textEditor.italic
                        onClicked: textEditor.italic = !textEditor.italic
                        focusPolicy: Qt.TabFocus
                        focus: false
                        opacity: activeFocus || hovered ? 1 : 0.6
                    }
                    Kube.IconButton {
                        iconName: Kube.Icons.underline
                        checkable: true
                        checked: textEditor.underline
                        onClicked: textEditor.underline = !textEditor.underline
                        focusPolicy: Qt.TabFocus
                        focus: false
                        opacity: activeFocus || hovered ? 1 : 0.6
                    }
                    Kube.TextButton {
                        id: deleteButton
                        text: qsTr("Remove Formatting")
                        visible: textEditor.htmlEnabled
                        onClicked: textEditor.clearFormatting()
                        opacity: activeFocus || hovered ? 1 : 0.6
                    }
                }

                Item {
                    height: 1
                    Layout.fillWidth: true
                }

                Component {
                    id: detailsComponent
                    Kirigami.Page {
                        title: qsTr("Details")

                        topPadding: 0
                        bottomPadding: 0
                        leftPadding: 0
                        rightPadding: 0

                        //Recipients
                        FocusScope {
                            id: recipients
                            Layout.fillHeight: true
                            width: Kube.Units.gridUnit * 15
                            activeFocusOnTab: true

                            //background
                            Rectangle {
                                anchors.fill: parent
                                color: Kube.Colors.backgroundColor

                                Rectangle {
                                    height: parent.height
                                    width: 1
                                    color: Kube.Colors.buttonColor
                                }
                            }

                            //Content
                            ColumnLayout {
                                anchors {
                                    fill: parent
                                    margins: Kube.Units.largeSpacing
                                }

                                spacing: Kube.Units.largeSpacing
                                ColumnLayout {
                                    Layout.maximumWidth: parent.width
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true

                                    Kube.Label {
                                        text: qsTr("Sending copy to (Cc):")
                                    }
                                    AddresseeListEditor {
                                        id: cc
                                        Layout.preferredHeight: cc.implicitHeight
                                        Layout.fillWidth: true
                                        activeFocusOnTab: true
                                        encrypt: composerController.encrypt
                                        controller: composerController.cc
                                        completer: composerController.recipientCompleter
                                    }

                                    Kube.Label {
                                        text: qsTr("Sending secret copy to (Bcc):")
                                    }
                                    AddresseeListEditor {
                                        id: bcc
                                        Layout.preferredHeight: bcc.implicitHeight
                                        Layout.fillWidth: true
                                        activeFocusOnTab: true
                                        encrypt: composerController.encrypt
                                        controller: composerController.bcc
                                        completer: composerController.recipientCompleter
                                    }
                                    Item {
                                        width: parent.width
                                        Layout.fillHeight: true
                                    }
                                }

                                RowLayout {
                                    enabled: composerController.foundPersonalKeys
                                    Kube.CheckBox {
                                        id: encryptCheckbox
                                        checked: composerController.encrypt
                                    }
                                    Kube.Label {
                                        text: qsTr("encrypt")
                                    }
                                }

                                RowLayout {
                                    enabled: composerController.foundPersonalKeys
                                    Kube.CheckBox {
                                        id: signCheckbox
                                        checked: composerController.sign
                                    }
                                    Kube.Label {
                                        text: qsTr("sign")
                                    }
                                }
                                Kube.Label {
                                    visible: !composerController.foundPersonalKeys
                                    Layout.maximumWidth: parent.width
                                    text: qsTr("Encryption is not available because your personal key has not been found.")
                                    wrapMode: Text.Wrap
                                }

                                RowLayout {
                                    Layout.maximumWidth: parent.width
                                    width: parent.width
                                    height: Kube.Units.gridUnit

                                    Kube.Button {
                                        width: saveDraftButton.width
                                        text: qsTr("Discard")
                                        onClicked: root.done()
                                    }

                                    Kube.Button {
                                        id: saveDraftButton

                                        text: qsTr("Save as Draft")
                                        enabled: composerController.saveAsDraftAction.enabled
                                        onClicked: {
                                            composerController.saveAsDraftAction.execute()
                                        }
                                    }
                                }

                                ColumnLayout {
                                    Layout.maximumWidth: parent.width
                                    Layout.fillWidth: true
                                    Kube.Label {
                                        id: fromLabel
                                        text: qsTr("You are sending this from:")
                                    }

                                    Kube.ComboBox {
                                        id: identityCombo
                                        objectName: "identityCombo"

                                        width: parent.width - Kube.Units.largeSpacing * 2

                                        model: composerController.identitySelector.model
                                        textRole: "address"
                                        Layout.fillWidth: true
                                        //A regular binding is not enough in this case, we have to use the Binding element
                                        Binding { target: identityCombo; property: "currentIndex"; value: composerController.identitySelector.currentIndex }
                                        onCurrentIndexChanged: {
                                            composerController.identitySelector.currentIndex = currentIndex
                                        }
                                    }
                                }

                                Kube.PositiveButton {
                                    objectName: "sendButton"
                                    id: sendButton

                                    width: parent.width

                                    text: qsTr("Send")
                                    enabled: composerController.sendAction.enabled
                                    onClicked: {
                                        composerController.sendAction.execute()
                                    }
                                }
                            }
                        }//FocusScope
                    }
                }

                Kube.Button {
                    text: qsTr("Attach file")

                    onClicked: {
                        fileDialog.open()
                    }
                    opacity: activeFocus || hovered ? 1 : 0.6

                    Dialogs.FileDialog {
                        id: fileDialog
                        title: qsTr("Choose a file to attach")
                        folder: shortcuts.home
                        selectFolder: false
                        selectExisting: true
                        selectMultiple: true
                        onAccepted: {
                            for (var i = 0; i < fileDialog.fileUrls.length; ++i) {
                                controller.attachments.add({url: fileDialog.fileUrls[i]})
                            }
                        }
                    }
                }
            }

            Kube.Label {
                text: qsTr("Sending email to:")
            }

            AddresseeListEditor {
                Layout.preferredHeight: implicitHeight
                Layout.fillWidth: true
                focus: true
                activeFocusOnTab: true
                encrypt: composerController.encrypt
                controller: composerController.to
                completer: composerController.recipientCompleter
            }

            Kube.HeaderField {
                id: subject
                objectName: "subject"
                Layout.fillWidth: true
                activeFocusOnTab: true
                focus: true

                placeholderText: qsTr("Subject")
                text: controller.subject
                onTextChanged: controller.subject = text;
                /*onActiveFocusChanged: {
                    if (activeFocus) {
                        root.focusChange()
                    }
                }*/
                onAccepted: textEditor.forceActiveFocus(Qt.TabFocusReason)
            }

            Kube.TextEditor {
                id: textEditor
                objectName: "textEditor"
                activeFocusOnTab: true
                placeholderText: qsTr("Body")

                Layout.fillWidth: true
                Layout.fillHeight: true

                border.width: 0

                onHtmlEnabledChanged: {
                    controller.htmlBody = htmlEnabled;
                }

                //onActiveFocusChanged: root.focusChange() #TODO remove properly
                Keys.onEscapePressed: root.done()
                onTextChanged: {
                    controller.body = text;
                }
            }
            Kube.Button {
                text: qsTr("continue")
                onClicked: root.pageRow.push(detailsComponent)
            }
        }
    }
}
