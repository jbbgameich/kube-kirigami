/*
 *  Copyright (C) 2017 Michael Bohlender, <michael.bohlender@kdemail.net>
 *  Copyright (C) 2017 Christian Mollekopf, <mollekopf@kolabsys.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.0 as Dialogs

import org.kde.kirigami 2.2 as Kirigami
import org.kube.framework 1.0 as Kube

Kube.View {
    id: root

    visibleViews: 2
    property bool newMessage: false
    property int loadType: Kube.ComposerController.Draft
    property variant message: {}
    property variant recipients: []
    property variant accountId: {}

    resources: [
        Kube.ComposerController {
            id: composerController
            objectName: "composerController"
            sign: signCheckbox.enabled && signCheckbox.checked
            encrypt: encryptCheckbox.enabled && encryptCheckbox.checked
            onDone: root.done()

            property bool foundAllKeys: composerController.to.foundAllKeys && composerController.cc.foundAllKeys && composerController.bcc.foundAllKeys

            sendAction.enabled: composerController.accountId &&
                                composerController.subject &&
                                (!composerController.encrypt || composerController.foundAllKeys) &&
                                (!composerController.sign && !composerController.encrypt || composerController.foundPersonalKeys) &&
                                !composerController.to.empty
            saveAsDraftAction.enabled: composerController.accountId
            onMessageLoaded: { editorPage.initialText = body }
            onCleared: { editorPage.initialText = "" }
        }
    ]

    onSetup: {
        loadMessage(root.message, root.loadType)
        if (root.accountId) {
            composerController.identitySelector.currentAccountId = root.accountId
        }
    }

    onRefresh: {
        Kube.Fabric.postMessage(Kube.Messages.synchronize, {"type": "mail", "specialPurpose": "drafts"})
        //For autocompletion
        Kube.Fabric.postMessage(Kube.Messages.synchronize, {"type": "contacts"})
    }

    onAborted: {
        //Avoid loosing the message
        if (composerController.saveAsDraftAction.enabled) {
            composerController.saveAsDraftAction.execute()
        }
    }

    function loadMessage(message, loadType) {
        if (message) {
            switch(loadType) {
                case Kube.ComposerController.Draft:
                    composerController.loadDraft(message)
                    break;
                case Kube.ComposerController.Reply:
                    composerController.loadReply(message)
                    editorPage.forceActiveFocus()
                    break;
                case Kube.ComposerController.Forward:
                    composerController.loadForward(message)
                    editorPage.forceActiveFocus()
                    break;
            }

        } else if (newMessage) {
            composerController.clear()
            if (root.recipients) {
                for (var i = 0; i < root.recipients.length; ++i) {
                    composerController.to.add({name: root.recipients[i]})
                }
            }
            editorPage.forceActiveFocus()
        }
    }

    function closeFirstSplitIfNecessary() {
        //Move the view forward
        if (root.currentIndex == 0) {
            root.incrementCurrentIndex()
        }
    }

    Kirigami.PageRow {
        id: row
        globalToolBar.style: Kirigami.ApplicationHeaderStyle.Auto
        Layout.fillWidth: true
        Layout.fillHeight: true
        clip: true

        Component.onCompleted: {
            push(draftsComponent)
            push(editorComponent, {"pageRow": row})
            //push(detailsComponent)
        }

        //Drafts
        Component {
            id: draftsComponent

            Kirigami.Page {
                title: qsTr("Drafts")

                topPadding: 0
                bottomPadding: 0
                leftPadding: 0
                rightPadding: 0

                Rectangle {
                    /*width: Kube.Units.gridUnit * 15
                    Layout.minimumWidth: Kube.Units.gridUnit * 5
                    Layout.fillHeight: true*/

                    anchors.fill: parent

                    color: Kube.Colors.darkBackgroundColor

                    ColumnLayout {

                        anchors {
                            fill: parent
                            topMargin: Kube.Units.largeSpacing
                            leftMargin: Kube.Units.largeSpacing
                        }

                        spacing: Kube.Units.largeSpacing

                        Kube.PositiveButton {
                            objectName: "newMailButton"

                            width: parent.width - Kube.Units.largeSpacing
                            focus: true
                            text: qsTr("New Email")
                            onClicked: {
                                listView.currentIndex = -1
                                composerController.clear()
                                editorPage.forceActiveFocus()
                            }
                        }

                        Kube.Label{
                            text: qsTr("Drafts")
                            color: Kube.Colors.highlightedTextColor
                            font.weight: Font.Bold
                        }

                        Kube.ListView {
                            id: listView
                            activeFocusOnTab: true

                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            clip: true
                            currentIndex: -1
                            highlightFollowsCurrentItem: false

                            //BEGIN keyboard nav
                            onActiveFocusChanged: {
                                if (activeFocus && currentIndex < 0) {
                                    currentIndex = 0
                                }
                            }
                            Keys.onDownPressed: {
                                listView.incrementCurrentIndex()
                            }
                            Keys.onUpPressed: {
                                listView.decrementCurrentIndex()
                            }
                            //END keyboard nav

                            onCurrentItemChanged: {
                                if (currentItem) {
                                    root.loadMessage(currentItem.currentData.domainObject, Kube.ComposerController.Draft)
                                }
                            }

                            model: Kube.MailListModel {
                                id: mailListModel
                                showDrafts: true
                            }

                            delegate: Kube.ListDelegate {
                                id: delegateRoot

                                color: Kube.Colors.darkBackgroundColor
                                border.width: 0

                                Item {
                                    id: content

                                    anchors {
                                        fill: parent
                                        margins: Kube.Units.smallSpacing
                                    }

                                    Kube.Label {
                                        width: content.width - Kube.Units.largeSpacing
                                        text: model.subject == "" ? "no subject" : model.subject
                                        color: Kube.Colors.highlightedTextColor
                                        maximumLineCount: 2
                                        wrapMode: Text.WrapAnywhere
                                        elide: Text.ElideRight
                                    }

                                    Kube.Label {
                                        anchors {
                                            right: parent.right
                                            rightMargin: Kube.Units.largeSpacing
                                            bottom: parent.bottom
                                        }
                                        text: Qt.formatDateTime(model.date, "dd MMM yyyy")
                                        font.italic: true
                                        color: Kube.Colors.disabledTextColor
                                        font.pointSize: Kube.Units.smallFontSize
                                        visible: !delegateRoot.hovered
                                    }
                                }
                                Row {
                                    id: buttons

                                    anchors {
                                        right: parent.right
                                        bottom: parent.bottom
                                        bottomMargin: Kube.Units.smallSpacing
                                        rightMargin: Kube.Units.largeSpacing
                                    }

                                    visible: delegateRoot.hovered
                                    spacing: Kube.Units.smallSpacing
                                    opacity: 0.7

                                    Kube.IconButton {
                                        id: deleteButton
                                        activeFocusOnTab: true
                                        iconName: Kube.Icons.moveToTrash
                                        visible: enabled
                                        enabled: !!model.mail
                                        onClicked: Kube.Fabric.postMessage(Kube.Messages.moveToTrash, {"mail": model.mail})
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Content
        Component {
            id: editorComponent

            Kirigami.Page {
                id: page
                title: qsTr("Editor")

                property var pageRow

                topPadding: 0
                bottomPadding: 0
                leftPadding: 0
                rightPadding: 0

                EditorPage {
                    id: editorPage

                    pageRow: page.pageRow
                    /*Layout.fillWidth: true
                    Layout.minimumWidth: Kube.Units.gridUnit * 5
                    Layout.fillHeight: true*/

                    anchors.fill: parent

                    controller: composerController
                    onDone: recipients.forceActiveFocus(Qt.TabFocusReason)
                    onFocusChange: closeFirstSplitIfNecessary()
                }
            }
        }
    }
}
